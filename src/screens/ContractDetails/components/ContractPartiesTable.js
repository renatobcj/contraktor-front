import React from 'react';
import {Avatar, Button, Table, Tag, Tooltip} from "antd"
import {IdcardOutlined, MailOutlined, PhoneOutlined} from "@ant-design/icons/es/icons/index"
import {truncate} from "../../../helpers/stringHelper"


const ContractPartiesTable = ({contract, removePartFromContractFn}) => {
    const columns = [
        {
            title: 'Nome',
            dataIndex: 'nome',
            key: 'nome',
            render: (text, record) => {
                const fullName = `${record.nome} ${record.sobrenome}`;

                return (
                    <Tooltip title={fullName.length > 15 ? fullName : 'Nome completo'} placement="top">
                        <Avatar size="small"
                                style={{backgroundColor: 'lightgrey', marginRight: 10}}> {record.nome.substr(0, 1).toUpperCase()}</Avatar>
                        {truncate(fullName, 15)}
                    </Tooltip>
                )
            }
        },
        {
            title: 'CPF',
            dataIndex: 'cpf',
            key: 'cpf',
            render: (text, record) => (
                <Tooltip title="CPF" placement="top">
                    <IdcardOutlined/> {record.cpf}
                </Tooltip>
            )
        },
        {
            title: 'Telefone',
            dataIndex: 'telefone',
            key: 'telefone',
            render: (text, record) => (
                <Tooltip title="Telefone" placement="top">
                    <PhoneOutlined/> {record.telefone}
                </Tooltip>
            )
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            render: (text, record) => (
                <Tooltip title="Email" placement="top">
                    <MailOutlined/> {record.email}
                </Tooltip>
            )
        },
        {
            title: 'Acoes',
            key: 'acoes',
            render: (text, record) => (
                <Button onClick={() => removePartFromContractFn(record.id)} danger type="text">Remover</Button>
            )
        },
    ];

    return (
        <div style={styles.container}>
            <Table
                showHeader={false}
                pagination={false}
                rowKey="id"
                columns={columns}
                dataSource={contract?.partes}
            />
        </div>
    )
};

const styles = {
    container: {
        marginTop: 12
    }
}

export default ContractPartiesTable;