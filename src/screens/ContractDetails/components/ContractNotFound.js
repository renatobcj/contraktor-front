import React from 'react';
import {Empty, Button} from 'antd';
import {FileSearchOutlined} from "@ant-design/icons/es/icons/index";
import {useHistory} from "react-router-dom";

const ContractNotFound = () => {
    let history = useHistory();


    return (
        <div style={styles.container}>
            <Empty
                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                imageStyle={{
                    height: 150,
                }}
                description={
                    <span>Contrato não encontrado.</span>
                }>
                <Button onClick={() => history.push("/")} icon={<FileSearchOutlined/>} type="primary">Listar contratos</Button>
            </Empty>
        </div>
    )
}

const styles = {
    container: {
        paddingTop: 200
    }
}

export default ContractNotFound;