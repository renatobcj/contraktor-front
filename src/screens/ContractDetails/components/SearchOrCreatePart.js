import React, {useState, useEffect} from 'react';
import {message} from "antd/lib/index"
import {Button, Select, Space} from 'antd';
import {CheckOutlined, PlusOutlined} from "@ant-design/icons/es/icons/index"
import CreatePart from "./CreatePart"

const {Option} = Select;

const SearchOrCreatePart = ({addPartToContractFn, parties, setActiveTabKey, findPartiesFn}) => {
    const [selectedPart, setSelectedPart] = useState(null);
    const [addingPart, setAddingPart] = useState(false);
    const [creatingPart, setCreatingPart] = useState(false);

    const addPartToContract = async () => {
        if (!selectedPart) {
            message.error("Nenhuma parte selecionada");
            return false;
        }

        setAddingPart(true);

        await addPartToContractFn(selectedPart);
        setActiveTabKey('parties')
        setSelectedPart(null);

        setAddingPart(false);
    }

    if (creatingPart)
        return <CreatePart findPartiesFn={findPartiesFn} setCreatingPart={setCreatingPart} addPartToContractFn={addPartToContractFn} setActiveTabKey={setActiveTabKey}/>

    return (
        <div style={styles.container}>
            <Space>
                <Select
                    value={selectedPart}
                    showClear={true}
                    showSearch
                    style={styles.select}
                    placeholder="Busque pelo nome da parte"
                    optionFilterProp="children"
                    onChange={setSelectedPart}
                    filterOption={(input, option) =>
                        option.children.join(" ").toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                >
                    {parties.map((part, i) => <Option key={i} value={part.id}>{part.nome} {part.sobrenome} - {part.cpf}</Option>)}
                </Select>
                {selectedPart && <Button loading={addingPart} onClick={addPartToContract} icon={<CheckOutlined/>}>Adicionar parte</Button>}
            </Space>
            <p style={styles.divider}>ou</p>
            <Button onClick={() => setCreatingPart(true)} icon={<PlusOutlined/>} type="primary">Cadastre uma nova parte</Button>
        </div>
    )
};

const styles = {
    container: {
        marginTop: 40,
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    divider: {
        margin: 15,
        opacity: 0.2
    },
    select: {width: 300}
}

export default SearchOrCreatePart;