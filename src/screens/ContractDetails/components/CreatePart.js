import React, {useState} from 'react';
import {Button, Input, Space} from "antd"
import {ArrowLeftOutlined, PlusOutlined} from "@ant-design/icons/es/icons/index"
import {message} from "antd/lib/index"
import apiInstance from "../../../config/apiInstance";
import InputMask from "react-input-mask";


const CreatePart = ({setCreatingPart, addPartToContractFn, setActiveTabKey, findPartiesFn}) => {
    const [firstName, setFirstName] = useState(null);
    const [lastName, setLastName] = useState(null);
    const [email, setEmail] = useState(null);
    const [cpf, setCpf] = useState(null);
    const [tel, setTel] = useState(null);


    const createPart = async () => {
        try {
            const response = await apiInstance.post(`/parties`, {
                nome: firstName,
                sobrenome: lastName,
                email: email,
                cpf: cpf,
                telefone: tel
            });
            const data = response.data;
            if (data.error) throw new Error(data.error);

            const createdPartId = data.id;
            await findPartiesFn();
            await addPartToContractFn(createdPartId);
            clearInputs();
            setActiveTabKey("parties");
        } catch (e) {
            console.log(e);
            message.error(e.message);
        }
    };

    const clearInputs = () => {
        setFirstName(null);
        setLastName(null);
        setEmail(null);
        setCpf(null);
        setTel(null);
    }

    return (
        <div style={styles.container}>
            <Space direction="vertical">
                <Space>
                    <Input value={firstName} onChange={(e) => setFirstName(e.target.value)} placeholder="Nome"/>
                    <Input value={lastName} onChange={(e) => setLastName(e.target.value)} placeholder="Sobrenome"/>
                </Space>
                <div>
                    <Input value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email"/>
                </div>
                <Space>
                    <InputMask mask="999.999.999-99" value={cpf} onChange={(e) => setCpf(e.target.value)}>
                        <Input placeholder="CPF"/>
                    </InputMask>
                    <InputMask mask="(99)99999-9999" value={tel} onChange={(e) => setTel(e.target.value)}>
                        <Input placeholder="Telefone"/>
                    </InputMask>
                </Space>
                <Space>
                    <Button onClick={() => setCreatingPart(false)} style={styles.button} icon={<ArrowLeftOutlined/>}>Voltar</Button>
                    <Button onClick={createPart} style={styles.button} icon={<PlusOutlined/>} type="primary">Cadastrar parte</Button>
                </Space>
            </Space>
        </div>
    )
};

const styles = {
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginTop: 30,
    },
    button: {
        marginTop: 20
    }
}

export default CreatePart;