import React, {useState, useEffect} from 'react';
import {useParams, useHistory} from "react-router-dom";
import {message} from "antd/lib/index"
import apiInstance, {API_URL} from "../../config/apiInstance"
import {PageHeader, Tabs, Button, Card, Popconfirm, Descriptions, Tag} from 'antd';
import ContractNotFound from "./components/ContractNotFound"
import {DeleteOutlined, DownloadOutlined, UserAddOutlined, UserOutlined} from "@ant-design/icons/es/icons/index"
import moment from "moment"
import {formatBytes} from "../../helpers/filesHelper"
import ContractPartiesTable from "./components/ContractPartiesTable"
import SearchOrCreatePart from "./components/SearchOrCreatePart"

const {TabPane} = Tabs;


const ContractDetails = () => {
    let {contractId} = useParams();
    let history = useHistory();

    const [contract, setContract] = useState(false);
    const [allParties, setAllParties] = useState([]);

    const [loadingContract, setLoadingContract] = useState(false);
    const [loadingContractFile, setLoadingContractFile] = useState(false);
    const [activeTabKey, setActiveTabKey] = useState("parties");

    const findParties = async () => {
        try {
            const response = await apiInstance.get(`/parties`);
            const data = response.data;
            if (data.error) throw new Error("Ocorreu um erro na busca de partes.");

            setAllParties(data);
        } catch (e) {
            message.error(e.message);
        }
    }

    const findContract = async () => {
        setLoadingContract(true);

        try {
            const response = await apiInstance.get(`/contracts/${contractId}`);
            const data = response.data;
            if (data.error) throw new Error("Ocorreu um erro na busca do contrato");

            setContract(data);
        } catch (e) {
            message.error(e.message);
        }

        setLoadingContract(false);
    };

    const deleteContract = async () => {
        try {
            const response = await apiInstance.delete(`/contracts/${contractId}`);
            const data = response.data;
            if (data.error) throw new Error("Ocorreu um erro na busca do contrato");

            history.push("/")
        } catch (e) {
            message.error(e.message);
        }
    }

    const downloadContract = async () => {
        setLoadingContractFile(true);
        window.open(API_URL + contract.arquivo.arquivo);
        setLoadingContractFile(false);
    }

    const addPartToContract = async (partId) => {
        try {
            const response = await apiInstance.post(`/contracts/part`, {
                contrato_id: parseInt(contractId),
                parte_id: parseInt(partId)
            });
            const data = response.data;
            if (data.error) throw new Error(data.error);

            await findContract()
        } catch (e) {
            message.error(e.message);
        }
    };

    const removePartFromContract = async (partId) => {
        try {
            const response = await apiInstance.patch(`/contracts/part`, {
                contrato_id: parseInt(contractId),
                parte_id: parseInt(partId)
            });
            const data = response.data;
            if (data.error) throw new Error(data.error);

            const newParties = contract.partes.filter((part) => part.id !== partId);
            setContract({...contract, partes: newParties});
        } catch (e) {
            message.error(e.message);
        }
    }

    const findPartObject = (partId) => {
        return allParties.filter((part) => part.id === partId);
    }

    useEffect(() => {
        findContract();
        findParties();
    }, []);


    if (!contract && !loadingContract) return <ContractNotFound/>

    return (
        <div style={styles.container}>
            <Card style={styles.contractContainer}>
                <PageHeader
                    onBack={() => history.push("/")}
                    title={contract.titulo}
                    extra={[
                        <Button onClick={downloadContract} loading={loadingContractFile} key="1" icon={<DownloadOutlined/>}>Baixar contrato</Button>,
                        <Popconfirm placement="bottom" title={"Tem certeza que gostaria de excluir esse contrato?"} onConfirm={deleteContract}
                                    okText="Sim" cancelText="Cancelar">
                            <Button key="2" type="danger" icon={<DeleteOutlined/>}>Excluir contrato</Button>
                        </Popconfirm>
                    ]}
                    footer={
                        <Tabs onChange={setActiveTabKey} activeKey={activeTabKey}>
                            <TabPane tab={<span><UserOutlined/>Partes</span>} key="parties">
                                <ContractPartiesTable removePartFromContractFn={removePartFromContract} contract={contract}/>
                            </TabPane>
                            <TabPane tab={<span><UserAddOutlined/>Adicionar parte</span>} key="create-part">
                                <SearchOrCreatePart findPartiesFn={findParties} setActiveTabKey={setActiveTabKey} parties={allParties}
                                                    addPartToContractFn={addPartToContract}/>
                            </TabPane>
                        </Tabs>
                    }
                >
                    <div style={styles.contractDescription}>
                        <Descriptions size="small" column={3}>
                            <Descriptions.Item label="Criado em">{moment(contract.criado_em).format("DD/MM/YYYY")}</Descriptions.Item>
                            <Descriptions.Item label="Vigência Início">{moment(contract.vigencia_inicio).format("DD/MM/YYYY")}</Descriptions.Item>
                            <Descriptions.Item label="Vigência Fim">{moment(contract.vigencia_fim).format("DD/MM/YYYY")}</Descriptions.Item>
                            <Descriptions.Item label="Tipo do arquivo"> <Tag color="red">{contract?.arquivo?.tipo}</Tag></Descriptions.Item>
                            <Descriptions.Item label="Tamanho">{formatBytes(contract?.arquivo?.bytes)}</Descriptions.Item>
                        </Descriptions>
                    </div>
                </PageHeader>
            </Card>
        </div>
    )
};

const styles = {
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    contractContainer: {
        width: 900,
        marginTop: 20
    },
    contractDescription: {
        textAlign: "left",
        marginTop: 30,
        marginBottom: 40
    }
}

export default ContractDetails;