import React, {useState} from 'react';
import {useHistory} from "react-router-dom";
import {Modal, Button, Upload, message, Input, Space} from 'antd';
import {InboxOutlined} from "@ant-design/icons/es/icons/index";
import {DatePicker} from 'antd';
import apiInstance from "../../../config/apiInstance"

const {Dragger} = Upload;


const CreateContract = () => {
    let history = useHistory();
    const [loading, setLoading] = useState(false);
    const [title, setTitle] = useState(null);
    const [dateStart, setDateStart] = useState(false);
    const [dateEnd, setDateEnd] = useState(false);
    const [file, setFile] = useState([]);

    const cancelCreation = () => {
        history.push("/");
    };

    const createContract = async () => {
        setLoading(true);

        try {

            if (file.length === 0 || !title || !dateStart || !dateEnd) {
                throw new Error("Preencha todos os campos antes de continuar");
            }

            let formData = new FormData();
            formData.append("file", file[0]);
            formData.append("titulo", title);
            formData.append("vigencia_inicio", dateStart.format());
            formData.append("vigencia_fim", dateEnd.format());

            const response = await apiInstance.post("/contracts", formData);
            const data = response.data;
            if (data.error) throw new Error(data.error);

            const contractId = data.id;
            history.push(`/contracts/${contractId}`)
        } catch (e) {
            message.error(e.message);
        }
        setLoading(false);
    };

    const onFileChange = async (file) => {
        setFile([file]);
        return false;
    };

    const onFileRemove = (file) => {
        setFile([]);
    }

    return (
        <Modal
            visible={true}
            title="Criar novo contrato"
            onOk={createContract}
            onCancel={cancelCreation}
            footer={[
                <Button key="back" onClick={cancelCreation}>
                    Cancelar
                </Button>,
                <Button key="submit" type="primary" loading={loading} onClick={createContract}>
                    Criar contrato
                </Button>,
            ]}
        >
            <Space style={{width: '100%'}} direction="vertical">
                <div>
                    <span>Título do contrato:</span>
                    <Input value={title} onChange={(e) => setTitle(e.target.value)}/>
                </div>
                <div style={{display: "flex", flexDirection: "row", flex: 1}}>
                    <Space style={{width: "100%"}}>
                        <div style={{display: "flex", flexDirection: "column", flex: 1}}>
                            <span>Início vigência:</span>
                            <DatePicker value={dateStart} onChange={setDateStart} format={"DD/MM/YYYY"}/>
                        </div>
                        <div style={{display: "flex", flexDirection: "column", flex: 1}}>
                            <span>Fim vigência:</span>
                            <DatePicker value={dateEnd} onChange={setDateEnd} format={"DD/MM/YYYY"}/>
                        </div>
                    </Space>
                </div>
                <Dragger onRemove={onFileRemove} style={{marginTop: 20}} fileList={file} name={"file"} beforeUpload={onFileChange}>
                    <p className="ant-upload-drag-icon">
                        <InboxOutlined/>
                    </p>
                    <p className="ant-upload-text">Clique ou arraste seus arquivos nessa área.</p>
                    <p className="ant-upload-hint">
                        Formatos válidos: pdf, doc
                    </p>
                </Dragger>
            </Space>
        </Modal>
    )
};

export default CreateContract;