import React from 'react';
import {Avatar, Table, Tag, Tooltip} from "antd"
import {Link} from "react-router-dom"
import moment from 'moment';
import {CalendarOutlined} from "@ant-design/icons/es/icons/index"
import {formatBytes} from "../../../helpers/filesHelper"
import {truncate} from "../../../helpers/stringHelper"

const ContractsTable = ({contracts, loadingContracts}) => {
    const columns = [
        {
            title: 'Título',
            dataIndex: 'titulo',
            key: 'titulo',
            render: (text, record) => (
                <Tooltip title={text} placement="top">
                    <Link to={`/contracts/${record.id}`}>{truncate(text, 15)}</Link>
                </Tooltip>
            )

        },
        {
            title: 'Início',
            dataIndex: 'vigencia_inicio',
            key: 'vigencia_inicio',
            render: (text) => (
                <p><CalendarOutlined/> {moment(text).format("DD/MM/YYYY")}</p>
            ),
            sorter: (a, b) => new Date(b.vigencia_inicio) - new Date(a.vigencia_inicio)
        },
        {
            title: 'Vencimento',
            dataIndex: 'vigencia_fim',
            key: 'vigencia_fim',
            render: (text) => (
                <p><CalendarOutlined/> {moment(text).format("DD/MM/YYYY")}</p>
            ),
            sorter: (a, b) => new Date(b.vigencia_fim) - new Date(a.vigencia_fim),
        },
        {
            title: 'Partes',
            key: 'partes',
            render: (record) => {
                if (record.partes.length === 0) return "-"
                return (
                    <Avatar.Group maxCount={2} maxStyle={{color: '#f56a00', backgroundColor: '#fde3cf'}}>
                        {record.partes.map((parte, i) => (
                            <Tooltip key={i} title={`${parte.nome} ${parte.sobrenome}`} placement="top">
                                <Avatar style={{backgroundColor: '#f56a00'}}>{parte.nome.substr(0, 1).toUpperCase()}</Avatar>
                            </Tooltip>
                        ))}
                    </Avatar.Group>
                )
            },
        },
        {
            title: "Arquivo",
            dataIndex: 'arquivo',
            key: 'arquivo',
            render: (record) => {
                return (
                    <Tooltip title={formatBytes(record.bytes)} placement="top">
                        <Tag color="red">{record.tipo}</Tag>
                    </Tooltip>
                )
            }
        },
        {
            title: 'Detalhes',
            key: 'detalhes',
            render: (record) => {
                return (<Link to={`/contracts/${record.id}`}>Ver mais</Link>)
            }
        },
    ];

    return (
        <Table
            rowKey="id"
            columns={columns}
            dataSource={contracts}
            loading={loadingContracts}
        />
    )
};

export default ContractsTable;