import React, {useState, useEffect} from 'react';
import ContractsTable from "./components/ContractsTable";
import {Button, message,} from 'antd';
import apiInstance from "../../config/apiInstance";
import {PlusOutlined} from "@ant-design/icons/es/icons/index"
import {Route, Link} from "react-router-dom";
import CreateContract from "./components/CreateContract"

const LOGO_URL = "https://contraktor.com.br/wp-content/uploads/2020/04/cropped-Sem-T%C3%ADtulo-1.png";

const ListContracts = () => {
    const [contracts, setContracts] = useState([]);
    const [loadingContracts, setLoadingContracts] = useState(false);

    const findContracts = async () => {
        setLoadingContracts(true);

        try {
            const response = await apiInstance.get("/contracts");
            const data = response.data;
            if (data.error) throw new Error("Ocorreu um erro na busca dos contratos");

            setContracts(data);
        } catch (e) {
            message.error(e.message);
        }

        setLoadingContracts(false);
    };

    useEffect(() => {
        findContracts();
    }, []);

    return (
        <div style={styles.container}>
            <Route path={"/create-contract"}>
                <CreateContract/>
            </Route>
            <div style={styles.table}>
                <div style={styles.logo}><img src={LOGO_URL}/></div>
                <div style={styles.actionButtons}>
                    <Link to={"/create-contract"}>
                        <Button type="primary" icon={<PlusOutlined/>}>Criar novo contrato</Button>
                    </Link>
                </div>
                <ContractsTable contracts={contracts} loadingContracts={loadingContracts}/>
            </div>
        </div>
    )
};

const styles = {
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20
    },
    logo: {
        marginTop: 25,
        marginBottom: 40
    },
    actionButtons: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: 15
    },
    table: {
        width: 900,
        maxWidth: 1500,
    }
}

export default ListContracts;