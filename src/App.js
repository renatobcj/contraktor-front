import React from 'react';
import './App.css';
import {Route, BrowserRouter as Router, Switch} from "react-router-dom"
import ListContracts from "./screens/ListContracts/ListContracts"
import {ConfigProvider} from "antd"
import pt_BR from "antd/es/locale/pt_BR"
import 'moment/locale/pt-br';
import 'antd/dist/antd.css';
import ContractDetails from "./screens/ContractDetails/ContractDetails"

function App() {
    return (
        <ConfigProvider locale={pt_BR}>
            <div className="App">
                <Router>
                    <Switch>
                        <Route path={'/contracts/:contractId'}>
                            <ContractDetails/>
                        </Route>
                        <Route path={['/', '/contracts']}>
                            <ListContracts/>
                        </Route>
                    </Switch>
                </Router>
            </div>
        </ConfigProvider>
    );
}

export default App;
